import yaml

from class_generator import ClassGenerator
from class_member_creator import ClassMemberCreator
from class_method_creator import ClassMethodCreator


class CreateClassesFromYaml:
    """ create a python classes from a Yaml file (can be easier writen) """

    def __init__(self,
                 path_source_file: str,
                 save_file_folder_path: str):
        try:
            # try to read file
            with open(path_source_file) as document:
                data = yaml.load(document, Loader=yaml.FullLoader)
                for class_name in data:
                    # load class names
                    try:
                        comments = data[class_name]['comments']
                    except Exception as error:
                        raise Exception("Class name(s) is/are missing")

                    self._new_class = CreateClassFromYaml(class_name, comments)

                    # load class properties
                    try:
                        properties_data = data[class_name]['properties']
                        self.scan_properties(properties_data)
                    except Exception as error:
                        raise Exception("No properties defined for class")

                    # load class methods
                    try:
                        methods_data = data[class_name]['methods']
                        self.scan_methods(methods_data)
                    except Exception as error:
                        raise Exception("No methods defined for class")

                    # generate python class source file
                    self._new_class.create_source(save_file_path=save_file_folder_path)

        except Exception as error:
            raise Exception("Error at CreateClassesFromYaml, __init__, saying: {}".format(error))

    def scan_properties(self,
                        properties_data: list) -> None:
        """
        check the properties for there inner settings
        :param properties_data: a list of property objects as string format
        :return: build properties in class
        """
        for member in properties_data:
            member_name = next(iter(member))
            member_data = member.get(member_name)
            member_type = "Object"
            member_getter = True
            member_setter = True
            for key, value in member_data.items():
                if key == "type":
                    member_type = value
                elif key == "getter":
                    member_getter = value
                elif key == "setter":
                    member_setter = value
            self._new_class.add_property(member_name=member_name,
                                         p_type=member_type,
                                         getter=member_getter,
                                         setter=member_setter)

    def scan_methods(self, methods_data):
        """
        check the methods for there inner settings
        :param methods_data: a list of methods objects as string format
        :return: build methods in class
        """
        for method in methods_data:
            method_name = next(iter(method))
            method_data = method.get(method_name)
            method_return_type = "Object"
            method_arguments = []
            method_static = True
            method_comments = "TODO: Please complete comments here. "
            for key, value in method_data.items():
                if key == "return":
                    method_return_type = value
                elif key == "arguments":
                    for arg_dict in value:
                        for arg in arg_dict:
                            method_arguments.append(arg)
                elif key == "static":
                    method_static = value
                elif key == "comments":
                    method_comments = value
            self._new_class.add_method(
                method_name, method_arguments, method_return_type, method_comments, method_static)


class CreateClassFromYaml:
    """ create a python (single) class from a Yaml file (can be easier writen) """

    def __init__(self, name, comments):
        self._name = name
        self._comments = comments
        self._properties = []
        self._methods = []

    def add_property(self,
                     member_name: str,
                     p_type: str,
                     getter: bool,
                     setter: bool):
        """ add a member to class later, build from string """
        self._properties.append(ClassMemberCreator(member_name=member_name,
                                                   member_type=p_type,
                                                   need_get=getter,
                                                   need_set=setter))

    def add_method(self,
                   method_name: str,
                   m_arguments: list,
                   return_type: str,
                   comments: str,
                   is_static: bool):
        """ add a method to class later, build from string """
        self._methods.append(ClassMethodCreator(name=method_name,
                                                arguments=m_arguments,
                                                return_type=return_type,
                                                comment=comments,
                                                is_static=is_static))

    def create_source(self, save_file_path: str):
        ClassGenerator.create_full_class(save_path=save_file_path,
                                         class_name=self._name,
                                         class_message=self._comments,
                                         class_members=self._properties,
                                         class_methods=self._methods)


if __name__ == '__main__':
    CreateClassesFromYaml("classes_example.yml", "classes_example.yml")