# library imports
import re
import os

# project imports
from class_member_creator import ClassMemberCreator
from class_method_creator import ClassMethodCreator

# consts #
TAB = "    "
TAB2 = "{}{}".format(TAB, TAB)
TAB3 = "{}{}".format(TAB2, TAB)
# end - consts #


class ClassGenerator:
    """ create structure for a new classes """

    def __init__(self):
        pass

    @staticmethod
    def create_class(save_path: str,
                     class_name: str,
                     class_message: str,
                     class_members_names: list,
                     need_getters: bool,
                     need_setters: bool,
                     methods_names: list):
        """ generate empty style class with declarations of members and methods """
        class_name_old = class_name[0].upper() + class_name[1:].lower().replace(" ", "_")
        class_name_old = re.sub("[^a-zA-Z_]", "", class_name_old)
        class_name = ""
        # fix the class name to file standard
        for index in range(len(class_name_old)):
            if class_name_old[index] == "_":
                continue
            if index > 0 and class_name_old[index-1] == "_":
                class_name += class_name_old[index].upper()
            else:
                class_name += class_name_old[index]

        # class with docstring
        answer = "class {}:\n".format(class_name)
        answer += "{}\"\"\" {} \"\"\"\n\n".format(TAB, class_message)
        # constructor and members
        if isinstance(class_members_names, list) and len(class_members_names) > 0:
            answer += "{}def __init__(self, {}):\n".format(TAB, ", ".join([member_name for member_name in class_members_names]))
            for member_name in class_members_names:
                answer += "{}self._{} = {}\n".format(TAB2, member_name.lower(), member_name.lower())
            answer += "\n"
        else:
            answer += "{}def __init__(self):\n{}pass\n\n".format(TAB, TAB2)

        # getters
        if need_getters:
            answer += "{}# ---> getters <--- # \n\n".format(TAB)
            for member_name in class_members_names:
                answer += "{}def get_{}(self) -> object:\n{}return self._{}\n\n".format(TAB, member_name.lower(), TAB2,  member_name.lower())
            answer += "{}# ---> end - getters <--- # \n\n".format(TAB)

        # setters
        if need_setters:
            answer += "{}# ---> setters <--- # \n\n".format(TAB)
            for member_name in class_members_names:
                answer += "{}def set_{}(self, new_{}: object) -> None:\n{}self._{} = new_{}\n\n".format(TAB, member_name.lower(), member_name.lower(), TAB2,  member_name.lower(),  member_name.lower())
            answer += "{}# ---> end - setters <--- # \n\n".format(TAB)

        # methods
        answer += "{}# ---> logic <--- # \n\n".format(TAB)
        if isinstance(methods_names, list) and len(methods_names) > 0:
            for method_name in methods_names:
                answer += "{}def {}(self) -> object:\n{}pass\n\n".format(TAB, method_name.lower(), TAB2)
        answer += "{}# ---> end - logic <--- # \n".format(TAB)

        # save class into_file_path:
        with open(os.path.join(save_path, class_name.lower() + ".py"), "w") as class_file:
            class_file.write(answer)

    @staticmethod
    def create_full_class(save_path: str,
                          class_name: str,
                          class_message: str,
                          class_members: list,
                          class_methods: list,
                          deretive_class_name=None):
        """ generate full style class with declarations of members and methods """
        class_name_old = class_name[0].upper() + class_name[1:].lower().replace(" ", "_")
        class_name_old = re.sub("[^a-zA-Z_]", "", class_name_old)
        class_name = ""
        for index in range(len(class_name_old)):
            if class_name_old[index] == "_":
                continue
            if index > 0 and class_name_old[index-1] == "_":
                class_name += class_name_old[index].upper()
            else:
                class_name += class_name_old[index]

        # class with docstring
        answer = "# library imports\n\n# project imports\n\n\nclass"
        answer += " {}:\n".format(class_name) if deretive_class_name is None else " {}({}):\n".format(class_name, deretive_class_name)
        answer += "{}\"\"\" {} \"\"\"\n\n".format(TAB, class_message)
        # constructor and members
        if isinstance(class_members, list) and len(class_members) > 0 and isinstance(class_members[0], ClassMemberCreator):
            answer += "{}def __init__(self, ".format(TAB)
            for member in class_members:
                answer += "{}: {}, ".format(member.get_member_name(), member.get_member_type())
            answer = answer[:-2] + "):\n"
            for member in class_members:
                answer += "{}self._{} = {}\n".format(TAB2, member.get_member_name(), member.get_member_name())
            answer += "\n"
        else:
            answer += "{}def __init__(self):\n{}pass\n\n".format(TAB, TAB2)

        # getters
        answer += "{}# ---> getters <--- # \n\n".format(TAB)
        for member in class_members:
            if member.is_need_get():
                answer += "{}def get_{}(self) -> {}:\n{}return self._{}\n\n".format(TAB,
                                                                                    member.get_member_name(),
                                                                                    member.get_member_type(),
                                                                                    TAB2,
                                                                                    member.get_member_name())
        answer += "{}# ---> end - getters <--- # \n\n".format(TAB)

        # setters
        answer += "{}# ---> setters <--- # \n\n".format(TAB)
        for member in class_members:
            if member.is_need_set():
                answer += "{}def set_{}(self, new_{}) -> None:\n{}self._{} = new_{}\n\n".format(TAB,
                                                                                                member.get_member_name(),
                                                                                                member.get_member_name(),
                                                                                                TAB2,
                                                                                                member.get_member_name(),
                                                                                                member.get_member_name())
        answer += "{}# ---> end - setters <--- # \n\n".format(TAB)

        # methods
        answer += "{}# ---> logic <--- # \n\n".format(TAB)
        if isinstance(class_methods, list) and len(class_methods) > 0 and isinstance(class_methods[0], ClassMethodCreator):
            for method in class_methods:
                answer += method.to_string()
        answer += "{}# ---> end - logic <--- # \n".format(TAB)

        # save class into_file_path:
        # save class into_file_path:
        with open(os.path.join(save_path, class_name.lower() + ".py"), "w") as class_file:
            class_file.write(answer)
