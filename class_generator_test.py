# library imports
import os
import unittest

# project imports
from class_generator import ClassGenerator, ClassMethodCreator, ClassMemberCreator


class ClassGeneratorTest(unittest.TestCase):
    """ unit test to check the ClassGenerator class """

    test_file_path = ""

    def test_regular_class_build(self):
        """ test the regular class build """
        right_answer = None
        try:
            right_answer = """class TestClass:
                                \"\"\" Build test class \"\"\"

                                def __init__(self, member1, member2):
                                    self._member1 = member1
                                    self._member2 = member2

                                # ---> getters <--- # 

                                def get_member1(self) -> object:
                                    return self._member1

                                def get_member2(self) -> object:
                                    return self._member2

                                # ---> end - getters <--- # 

                                # ---> setters <--- # 

                                def set_member1(self, new_member1: object) -> None:
                                    self._member1 = new_member1

                                def set_member2(self, new_member2: object) -> None:
                                    self._member2 = new_member2

                                # ---> end - setters <--- # 

                                # ---> logic <--- # 

                                def method1(self) -> object:
                                    pass

                                def method2(self) -> object:
                                    pass

                                # ---> end - logic <--- # 
                            """
            # build file
            ClassGenerator.create_class(save_path=ClassGeneratorTest.test_file_path,
                                        class_name="test class",
                                        class_message="Build test class",
                                        class_members_names=["member1", "member2"],
                                        need_getters=True,
                                        need_setters=True,
                                        methods_names=["method1", "method2"])
            # read answer
            with open(ClassGeneratorTest.test_file_path, "r") as class_file:
                read_answer = class_file.read()
            # check answer
            self.assertEqual(read_answer, right_answer)
        except Exception as error:
            self.assertEqual("", right_answer)
        finally:
            # in any case, remove file after check
            file_path = os.path.join(ClassGeneratorTest.test_file_path, "testclass.py")
            os.remove(file_path) if os.path.isfile(file_path) else None

    def test_full_class_build(self):
        """ test the full class build """
        right_answer = None
        try:
            right_answer = """# library imports
                            
                            # project imports
                            
                            
                            class TestClass:
                                \"\"\" test a full class creator \"\"\"
                            
                                def __init__(self, member1: str, member2: bool):
                                    self._member1 = member1
                                    self._member2 = member2
                            
                                # ---> getters <--- # 
                            
                                def get_member1(self) -> str:
                                    return self._member1
                            
                                # ---> end - getters <--- # 
                            
                                # ---> setters <--- # 
                            
                                def set_member1(self, new_member1) -> None:
                                    self._member1 = new_member1
                            
                                # ---> end - setters <--- # 
                            
                                # ---> logic <--- # 
                            
                                @staticmethod
                                def method1(arg1: object, arg2: object) -> str:
                                    \"\"\" method comments \"\"\"
                                    pass
                            
                                def method2(self) -> str:
                                    \"\"\" method comments 2 \"\"\"
                                    pass
                            
                                # ---> end - logic <--- # 
                            """
            # build file
            ClassGenerator.create_full_class(ClassGeneratorTest.test_file_path,
                                             "test class!",
                                             "test a full class creator",
                                             [ClassMemberCreator("member1", "str", True, True), ClassMemberCreator("member2", "bool", False, False)],
                                             [ClassMethodCreator("method1", ["arg1", "arg2"], "str", "method comments", True),
                                              ClassMethodCreator("method2", [], "str", "method comments 2", False)])
            # read answer
            with open(ClassGeneratorTest.test_file_path, "r") as class_file:
                read_answer = class_file.read()
            # check answer
            self.assertEqual(read_answer, right_answer)
        except Exception as error:
            self.assertEqual("", right_answer)
        finally:
            # in any case, remove file after check
            file_path = os.path.join(ClassGeneratorTest.test_file_path, "testclass.py")
            os.remove(file_path) if os.path.isfile(file_path) else None
