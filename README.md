# python_class_generator

An easy and fast way to develop your Object Oriented Python3 project using this meta-class generator.

# Introduction

An easy and fast way to develop your Object Oriented Python3 project using this meta-class generator. Reduce the time you west on writing the same technical code and focus on the logic itself!
The class generator is an native "next step" for python cause other leading programing languages like C# and Java have an auto getters / setters generators. One can find online even products of the shelf with "wizards" to fast develop classes in these languages.
Python has a status of a "script" language but this is not the case anymore as python used for big systems and complex algorithms in the last few years.

# Related Work

Our solution is based on ideas for class development design from the wonderful json to C# class generator tool.
Better performance and needs has been borrowed from the Entity Framework (EF) code design, so popular with pure object oriented programming when handling write and read from data bases (db).

# Installation and Deployment

The solution is a single class file. It does not use any dependencies as it is working on strings only.
Just include the file in your project file tree and call one of the two public methods.

# Main Features

1. Generated class stands the PEP8 code design
2. A quick structure class generator for quick and unclear development process
3. A full structure class generator for logic ready development process
4. A run time class generator for fully usable class with logic to run

# Uses
1. Quick and full structure class generator reduces development time and ensure high standard code.
2. Run time class generator is amazing for genetic programming and evaluation RML.

# License
Copyright (c) 2019 DnC-algo Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
